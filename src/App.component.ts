import { Component, Vue } from 'vue-property-decorator'
import Menu from './views/Menu/menu.component.vue'
import Carousel from './views/Carousel/carousel.component.vue'
import Content from './views/Content/content.component.vue'
import Footer from './views/Footer/footer.component.vue'

@Component({
  components: {
    Menu,
    Carousel,
    Content,
    Footer
  }
})
export default class AppVue extends Vue {

}
