// import { Component, Vue } from 'vue-property-decorator'
import {
  Hooper,
  Slide,
  Progress as HooperProgress,
  Pagination as HooperPagination,
  Navigation as HooperNavigation
} from 'hooper'
import 'hooper/dist/hooper.css'

// @Component({
//   components: {
//
//   }
// })
// export default class Carousel extends Vue {
//
// }

export default {
  components: {
    Hooper,
    Slide,
    HooperProgress,
    HooperPagination,
    HooperNavigation
  },

  data () {
    return {
      settings: {
        vertical: true,
        itemsToSlide: 1,
        itemsToShow: 1,
        centerMode: true,
        pagination: true
      }
    }
  }
}
