import { Component, Vue } from 'vue-property-decorator'

@Component({
  components: {
  }
})
export default class MenuComponent extends Vue {
  public show = false;

  /**
   * Show the menu if we click in
   * the button
   */

  public showMenu () {
    this.show = !this.show
  }
}
